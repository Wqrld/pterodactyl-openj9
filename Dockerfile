# ----------------------------------
# Pterodactyl Core Dockerfile
# Environment: Java (glibc support)
# Minimum Panel Version: 0.6.0
# ----------------------------------
FROM adoptopenjdk/openjdk8-openj9:alpine

MAINTAINER Isaac A. <isaac@isaacs.site>

RUN apk add --no-cache --update curl ca-certificates openssl git tar bash sqlite fontconfig \
 && adduser -D -h /home/container container \
 && ln -s /etc/localtime /etc/timezone

USER container
ENV  USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]